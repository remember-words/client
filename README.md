# Клиент для Remember words

## Сборка

Для получения зависимостей (один раз) потребуется NodeJS 16.x и выше.
Выполнить команду:
```
npm install
```

Для сборки и запуска клиента локально выполните команду:
```
npm run start
```

Для сборки docker образ:

```
docker build -t docker.io/library/remember-words-client:1.0 .
```