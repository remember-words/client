FROM node:16.13.1 as build
WORKDIR /usr/src/app
COPY package.json ./
COPY . ./
RUN --mount=type=cache,target=/usr/src/app/node_modules,rw npm i && npm run build

FROM nginx:latest
RUN mkdir /opt/rw
COPY --from=build /usr/src/app/dist /opt/rw