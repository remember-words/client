import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import {compose, createStore} from "redux"
import {Provider} from "react-redux"
import {BrowserRouter} from "react-router-dom"
import {rootReducer} from "./store"
import {MainAppPage} from "./pages/MainAppPage"

const composeEnhancers = (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
export const store = createStore(rootReducer, composeEnhancers())

ReactDOM.render(
    <React.StrictMode>
        <Provider store={store}>
            <BrowserRouter>
                <MainAppPage/>
            </BrowserRouter>
        </Provider>
    </React.StrictMode>,
    document.getElementById('app-root'),
)
