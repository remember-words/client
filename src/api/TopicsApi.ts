import axios, {AxiosResponse} from "axios"
import {Topic} from "../model/Topic"
import {EmptyRequest} from "./EmptyRequest"
import {DefaultResponse} from "./DefaultResponse"

const topicsUrl = "/api/v1/discovery/topics"

interface GetTopicsResponse extends DefaultResponse{
    topics: Array<Topic>
}

class TopicsApi {
    async getTopics(onLoad: (topics: Array<Topic>) => void) {
        return axios.get<EmptyRequest, AxiosResponse<GetTopicsResponse>>(topicsUrl)
            .then(serviceResponse => serviceResponse.data)
            .then(response => {
                if (response.success) {
                    onLoad(response.topics)
                } else {
                    console.log(response.message)
                }
            })
    }
}

export const topicsApi = new TopicsApi()
