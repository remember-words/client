import axios, {AxiosResponse} from "axios"
import {EmptyRequest} from "./EmptyRequest"
import {DefaultResponse} from "./DefaultResponse"

const baseUrl = "/api/v1/progress"
const userId: number = 1

interface UpdateProgressRequest {
    userId: number
    correct: boolean
}

interface GetProgressResponse extends DefaultResponse{
    level: number
}

class ProgressApi {
    async getProgress(onLoad: (level: number) => void) {
        return axios.get<EmptyRequest, AxiosResponse<GetProgressResponse>>(baseUrl + '/' + userId.toString())
            .then(response => response.data)
            .then(response => {
                if (response.success) {
                    onLoad(response.level)
                } else {
                    console.log(response.message)
                }
            })
    }

    async sendAnswer(correct: boolean, onLoad: () => void) {
        return axios.post<UpdateProgressRequest, AxiosResponse<DefaultResponse>>(baseUrl + '/update', {
            userId,
            correct
        }).then(response => response.data)
            .then(response => {
                if (response.success) {
                    onLoad()
                } else {
                    console.log(response.message)
                }
            })
    }
}

export const progressApi = new ProgressApi()
