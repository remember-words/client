import {Word} from "../model/Word"
import axios, {AxiosResponse} from "axios"
import {EmptyRequest} from "./EmptyRequest"
import {DefaultResponse} from "./DefaultResponse"

interface GetRandomResponse extends DefaultResponse{
    correctWord: number
    words: Array<Word>
}

class TrainerApi {
    async getRandomWords(serviceUrl: string, onLoad: (correctIndex: number, words: Array<Word>) => void) {
        const url = serviceUrl + '/api/v1/words/random-words'
        return axios.get<EmptyRequest, AxiosResponse<GetRandomResponse>>(url).then(response => response.data)
            .then(response => {
                if (response.success) {
                    onLoad(response.correctWord, response.words)
                } else {
                    console.log(response.message)
                }
            })
    }
}

export const trainerApi = new TrainerApi()
