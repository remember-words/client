import React, {FC, MouseEvent} from "react"
import {Topic} from "../../model/Topic"

interface TopicChooserProps {
    topic: Topic
    onChoose: (topic: Topic) => void
}

export const TopicChooser: FC<TopicChooserProps> = ({
                                                        topic,
                                                        onChoose
                                                    }) => {
    const chooseTopic = (event: MouseEvent<HTMLDivElement>) => {
        event.preventDefault()
        event.stopPropagation()
        onChoose(topic)
    }

    return (
        <div className={"topic" + (topic.available ? '' : ' disabled')} onClick={chooseTopic}>{topic.name}</div>
    )
}
