import React, {FC, useEffect} from "react"
import {useDispatch, useSelector} from "react-redux"
import {getLevel, isProgressLoaded} from "../../store/progress/selectors"
import {progressApi} from "../../api/ProgressApi"
import {loadProgress} from "../../store/progress/actions"
import './ProgressViewer.less'

export const ProgressViewer: FC = () => {
    const loaded = useSelector(isProgressLoaded)
    const level = useSelector(getLevel)
    const dispatch = useDispatch()

    useEffect(() => {
        if (!loaded) {
            progressApi.getProgress((level) => dispatch(loadProgress(level))).catch(console.error)
        }
    }, [loaded])

    return (
        <div className="progressViewer">
            {loaded && <div className="level">Ваш уровень: {level}</div>}
        </div>
    )
}
