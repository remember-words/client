import React, {FC, MouseEvent, useEffect, useState} from "react"
import {Topic} from "../model/Topic"
import {resetTopicSelection} from "../store/topics/actions"
import {useDispatch} from "react-redux"
import './TopicTrainer.less'
import {TrainerTask} from "../model/TrainerTask"
import {trainerApi} from "../api/TrainerApi"
import {ProgressViewer} from "../components/progress/ProgressViewer"
import {progressApi} from "../api/ProgressApi"
import {resetProgress} from "../store/progress/actions"

interface TopicTrainerProps {
    topic: Topic
}

export const TopicTrainer: FC<TopicTrainerProps> = ({topic}) => {
    const dispatch = useDispatch()
    const [task, setTask] = useState<TrainerTask | undefined>(undefined)
    const [message, setMessage] = useState("")
    const [wrongAnswer, setWrongAnswer] = useState(false)
    const [nextTask, setNextTask] = useState("")

    const goBack = (event: MouseEvent<HTMLButtonElement>) => {
        event.stopPropagation()
        event.preventDefault()
        dispatch(resetTopicSelection())
    }

    useEffect(() => {
        if (!task) {
            trainerApi.getRandomWords(topic.serviceUrl, (correctIndex, words) => {
                setTask({
                    word: words[correctIndex].original,
                    rightIndex: correctIndex,
                    words
                })
            }).catch(console.error)
        }
    }, [task, topic])

    const answer = (event: MouseEvent<HTMLDivElement>, index: number) => {
        event.stopPropagation()
        event.preventDefault()
        if (!task) {
            return
        }
        const correct = index === task.rightIndex
        if (correct) {
            setWrongAnswer(false)
            setMessage("Правильно!")
            setNextTask("Следующий вопрос через 2 секунды")
            setTimeout(() => {
                setTask(undefined)
                setMessage("")
                setNextTask("")
            }, 2000)
        } else {
            setWrongAnswer(true)
            setMessage("Неправильно!")
        }
        progressApi.sendAnswer(correct, () => dispatch(resetProgress())).catch(console.error)
    }

    return (
        <div className="topicTrainer">
            <div className="trainerHint">
                <button onClick={goBack}>Назад</button>
            </div>
            <ProgressViewer />
            {task && <div className="task">
                <div className="trainerWord">Выберите перевод для "{task.word}"</div>
                <div className="trainerAnswers">
                    {task.words && task.words.map((word, index) => (
                        <div key={"word_" + topic.id + word.id}
                             className="answer"
                             onClick={event => answer(event, index)}>{word.translated}</div>
                    ))}
                </div>
                <div className={"message" + (wrongAnswer ? ' error' : '')}>{message}</div>
                <div className="nextTaskMessage">{nextTask}</div>
            </div>}
        </div>
    )
}
