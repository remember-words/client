import React, {FC} from "react"
import {useSelector} from "react-redux"
import {getSelectedTopic} from "../store/topics/selectors"
import {TopicList} from "./TopicList"
import {TopicTrainer} from "./TopicTrainer"

export const MainAppPage: FC = () => {
    const selectedTopic = useSelector(getSelectedTopic)

    return (
        <div>
            {!selectedTopic && <TopicList />}
            {selectedTopic && <TopicTrainer topic={selectedTopic} />}
        </div>
    )
}
