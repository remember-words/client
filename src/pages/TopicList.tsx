import React, {FC, useEffect} from "react"
import {useDispatch, useSelector} from "react-redux"
import {getTopics, isTopicsLoaded} from "../store/topics/selectors"
import {topicsApi} from "../api/TopicsApi"
import {loadTopics, selectTopic} from "../store/topics/actions"
import {TopicChooser} from "../components/topic-list/TopicChooser"
import {Topic} from "../model/Topic"
import './TopicList.less'
import {ProgressViewer} from "../components/progress/ProgressViewer"

export const TopicList: FC = () => {
    const loaded = useSelector(isTopicsLoaded)
    const topics = useSelector(getTopics)
    const dispatch = useDispatch()

    useEffect(() => {
        if (!loaded) {
            topicsApi.getTopics(topics => dispatch(loadTopics(topics))).catch(console.error)
        }
    }, [loaded])

    const chooseTopic = (topic: Topic) => {
        dispatch(selectTopic(topic))
    }

    return (
        <div className="topicsPage">
            <ProgressViewer />
            <div className="topicsHint">Выберите тему для тренировки</div>
            <div className="topicList">
                {loaded && topics.map(topic => (
                    <TopicChooser key={"topic_" + topic.id} topic={topic} onChoose={chooseTopic} />
                ))}
            </div>
        </div>
    )
}
