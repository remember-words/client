export interface Topic {
    id: string
    name: string
    serviceUrl: string
    available: boolean
}
