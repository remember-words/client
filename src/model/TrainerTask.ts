import {Word} from "./Word"

export interface TrainerTask {
    word: string
    rightIndex: number
    words: Array<Word>
}