export interface Word {
    id: string
    original: string
    translated: string
}