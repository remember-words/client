import {Topic} from "../../model/Topic"

export enum TopicsActionType {
    LOAD = '[TOPICS] LOAD',
    SELECT = '[TOPICS] SELECT'
}

export interface TopicsAction {
    type: TopicsActionType
    topics?: Array<Topic>
    topic?: Topic
}

export interface TopicsState {
    loaded: boolean
    topics: Array<Topic>
    selectedTopic?: Topic
}

const initState: TopicsState = {
    loaded: false,
    topics: []
}

export const topicsReducer = (
    state: TopicsState = initState,
    action: TopicsAction
) => {
    switch (action.type) {
        case TopicsActionType.LOAD:
            return {
                ...state,
                loaded: true,
                topics: action.topics
            }
        case TopicsActionType.SELECT:
            return {
                ...state,
                selectedTopic: action.topic
            }
    }
    return state
}
