import {RootState} from "../index"
import {Topic} from "../../model/Topic"

export const isTopicsLoaded = (state: RootState): boolean => state.topics.loaded

export const getTopics = (state: RootState): Array<Topic> => state.topics.topics

export const getSelectedTopic = (state: RootState) => state.topics.selectedTopic
