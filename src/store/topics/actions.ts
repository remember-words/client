import {Topic} from "../../model/Topic"
import {TopicsAction, TopicsActionType} from "./index"

export const loadTopics = (topics: Array<Topic>): TopicsAction => {
    return ({
        type: TopicsActionType.LOAD,
        topics
    })
}

export const selectTopic = (topic: Topic): TopicsAction => {
    return ({
        type: TopicsActionType.SELECT,
        topic
    })
}

export const resetTopicSelection = (): TopicsAction => {
    return ({
        type: TopicsActionType.SELECT
    })
}
