import {combineReducers} from "redux"
import {topicsReducer, TopicsState} from "./topics"
import {progressReducer, ProgressState} from "./progress"

export interface RootState {
    topics: TopicsState
    progress: ProgressState
}

export const rootReducer = combineReducers({
    topics: topicsReducer,
    progress: progressReducer
})
