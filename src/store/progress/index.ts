export enum ProgressActionType {
    LOAD = "[PROGRESS] LOAD",
    RESET = "[PROGRESS] RESET"
}

export interface ProgressAction {
    type: ProgressActionType
    level?: number
}

export interface ProgressState {
    loaded: boolean
    level: number
}

const initState: ProgressState = {
    loaded: false,
    level: 0
}

export const progressReducer = (
    state: ProgressState = initState,
    action: ProgressAction
) => {
    switch (action.type) {
        case ProgressActionType.LOAD:
            return {
                ...state,
                loaded: true,
                level: action.level
            }
        case ProgressActionType.RESET:
            return {
                ...state,
                loaded: false
            }
    }
    return state
}
