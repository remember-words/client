import {RootState} from "../index"

export const isProgressLoaded = (state: RootState): boolean => state.progress.loaded

export const getLevel = (state: RootState): number => state.progress.level
