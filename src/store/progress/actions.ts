import {ProgressAction, ProgressActionType} from "./index"

export const loadProgress = (level: number): ProgressAction => {
    return ({
        type: ProgressActionType.LOAD,
        level
    })
}

export const resetProgress = (): ProgressAction => {
    return ({
        type: ProgressActionType.RESET
    })
}
